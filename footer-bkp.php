<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage aumoraes-profile
 * @since 1.0
 * @version 1.0
 */

?>

		<div id="footer" class="clearfix">
			<div class="footer-copy">
				<p class="copyright">© <?php echo date("Y"); ?> <a href="http://aumoraes.com" rel="home">Aurélio de Moraes</a></p>
			</div>
		</div>
	</div> <!-- .wrap (index)-->
</div> <!-- <div class="container-fluid header"> (header)-->

<?php wp_footer(); ?>

</body>

</html>
