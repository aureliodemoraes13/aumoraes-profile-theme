<div class="experiencia">

	<div class="page-header">
		<h1> <?php echo esc_html( pll__( 'experience' ) ) ?> </h1>
	</div>

	<?php
	$args = [
		'cat' => 10,
		'order_by' => 'created_at',
		'category_name' => 'experiencia',
		'order' => 'desc'
	];
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) :
		while ( $query->have_posts() ) : $query->the_post();
	?>

	<div class="item">
		<h2 class="company-name"><?php the_title(); ?></h2>
		<h4 class="colocation"><?php echo get_field("funcao") ?></h4>
		<h4 class="period">

			<?php echo date_i18n("M, Y", strtotime( get_field( "entrada" ) ) );	?>
			—
			<?php
			if( empty( get_field("saida") ) ){
				echo esc_html( pll__( 'current' ) );
			}else{
				echo date_i18n("M, Y", strtotime(get_field("saida")));
			}
			?>

		</h4>
		<p class="description">
			<?php echo get_the_content(); ?>
		</p>
	</div>

	<?php
	endwhile;
		wp_reset_postdata();
	endif;
	?>

</div>
